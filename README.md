# Laboratório com docusaurus

o objetivo deste projeto é servir de laboratório para a ferramenta de criação de páginas estáticas Docusaurus

### Teste local do projeto

para testar o projeto localmente, faça um clone do projeto:

```
$ git clone <clone-ssh-do-projeto>
```

e então digite o seguinte comando no seu terminal para instalar as dependencias:

```
$ npm i
```

e por ultimo, inicie o projeto com o seguinte comando: 

```
npm start
```


